﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LambdaPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> products = new List<string>() {"Basketball", "Baseball", "Tennis Raquet", "Running Shoes", "Wrestling Shoes", 
                "Soccer Ball", "Football", "Shoulder Pads", 
                "Trail Running Shoes", "Cycling Shoes", "Kayak", "Kayak Paddles"};

            //declare a variable kayakProducts and set it equal to all products that contain the word "Kayak"
            Console.WriteLine("#All Kayak products: ");
            var kayakProducts = products.Where(x => x.Contains("Kayak"));
            //print the kayakProducts to the console using a foreach loop.
            foreach (var kayakProduct in kayakProducts)
            {
                Console.WriteLine(kayakProduct);
            }
            Console.WriteLine();

            //declare a variable shoeProducts and set it equal to all products that contain the word "Shoes"
            Console.WriteLine("#All 'Shoe' products: ");
            var shoeProducts = products.Where(x => x.Contains("Shoes"));
            //print the shoeProducts to the console using a foreach loop or string.Join().
            foreach (var shoeProduct in shoeProducts)
            {
                Console.WriteLine(shoeProduct);
            }
            Console.WriteLine();

            //declare a variable ballProducts and set it equal to all the products that have ball in the name.
            Console.WriteLine("#All 'ball' products:");
            var ballProducts = products.Where(x => x.ToLower().Contains("ball"));
            //print the ballProducts to the console using a foreach loop or string.Join().
            foreach (var ballProduct in ballProducts)
            {
                Console.WriteLine(ballProduct);
            }
            Console.WriteLine();

            //sort ballProducts alphabetically and print them to the console.
            Console.WriteLine("#Sort ballProducts alphabetically: ");
            foreach (var ballProduct in ballProducts.OrderBy(x=>x))
            {
                Console.WriteLine(ballProduct);
            }
            Console.WriteLine();

            //print the product with the longest name to the console using the .First() extension.
            Console.WriteLine("#Longest Product: ");
            Console.WriteLine(products.OrderByDescending(x=>x.Length).First());
            Console.WriteLine();

            //print the product with the shortest name to the console using the .First() extension.
            Console.WriteLine("#Shortest Product: ");
            Console.WriteLine(products.OrderBy(x=>x.Length).First());
            Console.WriteLine();

            //print the product with the 3rd shortest name to the console using an index or Skip/Take (you must convert the results to a list using .ToList()).  
            Console.WriteLine("#3rd Shortest Name: ");
            Console.WriteLine(products.OrderBy(x=>x.Length).Skip(2).ToList().First());
            Console.WriteLine();

            //print the ballProduct with the 2nd longest name to the console using an index or Skip/Take (you must convert the results to a list using .ToList()). 
            Console.WriteLine("#2nd Longest Name:");
            Console.WriteLine(products.OrderByDescending(x => x.Length).Skip(1).ToList().First());
            Console.WriteLine();

            //declare a variable reversedProducts and set it equal to all products ordered by the longest word first. (use the OrderByDescending() extension).
            Console.WriteLine("#Revered products:");
            var reversedProducts = products.OrderByDescending(x => x.Length);
            //print out the reversedProducts to the console using a foreach loop.
            foreach (var reversedProduct in reversedProducts)
            {
                Console.WriteLine(reversedProduct);
            }
            Console.WriteLine();

            //print out all the products ordered by the longest word first using the OrderByDecending() extension and a foreach loop.
            foreach (var product in products.OrderByDescending(x=>x.Length))
            {
                Console.WriteLine(product);
            }
            //Note: you will not use a variable to store your list

            //FILL IN THE FUNCTIONS BELOW TO MAKE THE TESTS PASS

            Console.ReadKey();
        }

        public static string LongestName(List<string> inputList)
        {
            //with the input list, return the item with the longest name
            return inputList.OrderByDescending(x => x.Length).First();
        }

        public static string ShortestName(List<string> inputList)
        {
            //with the input list, return the item with the shortest name
            return inputList.OrderBy(x => x.Length).First();
        }

        public static string SecondLongestName(List<string> inputList)
        {
            //with the input list, return the item with the second longest name
            return inputList.OrderByDescending(x => x.Length).Skip(1).First();
        }

        public static string ThirdShortestName(List<string> inputList)
        {
            //with the input list, return the item with the third shortest name
            return inputList.OrderBy(x => x.Length).Skip(2).First();
        }

        public static List<string> BallProducts(List<string> inputList)
        {
            //with the input list, return a list with only the the products that contain the word ball
            return inputList.Where(x=>x.ToLower().Contains("ball")).ToList();
        }
        public static List<string> EndInS(List<string> inputList)
        {
            //with the input list, return a list with only the the products that end with the letter s
            return inputList.Where(x=>x.EndsWith("s")).ToList();
        }
    }
}